import React from 'react'
import AppContainer from './src/app/Router'

export default function App() {
  return <AppContainer />
}
