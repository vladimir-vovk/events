import React from 'react'
import { Image, Dimensions, View, Text, StyleSheet } from 'react-native'

type EventType = {
  id: string
  image: string
  title: string
  date: string
}

const WIDTH = Dimensions.get('window').width

const Event = ({ image, title, date }: EventType) => (
  <View style={styles.container}>
    <Image
      style={styles.image}
      source={{
        uri: image
      }}
    />
    <Text style={styles.title}>{title}</Text>
    <Text style={styles.date}>{date}</Text>
  </View>
)

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    marginBottom: 34,
    width: '100%'
  },
  image: {
    width: WIDTH * 0.75,
    height: WIDTH * 0.53,
    borderRadius: 12,
    marginBottom: 4
  },
  title: {
    fontSize: 13,
    lineHeight: 15,
    marginBottom: 4
  },
  date: {
    fontSize: 11,
    lineHeight: 13,
    color: '#9E9E9E'
  }
})

export default Event
export { EventType }
