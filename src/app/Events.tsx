import React from 'react'
import { View, Text, StyleSheet, SafeAreaView, FlatList } from 'react-native'
import { NavigationScreenProps } from 'react-navigation-stack'
import Event, { EventType } from './Event'

type Props = {
  navigation: NavigationScreenProps
}

const Empty = () => (
  <View style={styles.empty}>
    <Text>Nothing here yet</Text>
  </View>
)

const Events = ({ navigation }: Props) => {
  const events = navigation.getParam('events', [])

  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        ListHeaderComponent={events?.length ? <View /> : <Empty />}
        ListHeaderComponentStyle={styles.flatlist}
        data={events}
        keyExtractor={({ id }: EventType) => id}
        renderItem={({ item }) => <Event {...item} />}
      />
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  flatlist: {
    height: 49
  },
  empty: {
    height: 64,
    alignItems: 'center',
    justifyContent: 'center'
  }
})

Events.navigationOptions = {
  headerShown: false
}

export default Events
