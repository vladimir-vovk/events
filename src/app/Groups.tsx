import React, { useState } from 'react'
import { Text, View, StyleSheet } from 'react-native'
import { NavigationScreenProps } from 'react-navigation-stack'
import Button from '../core/ui/Button'
import GROUPS from './fakeData'
import { NavigationScreenProp } from 'react-navigation'

type Props = {
  navigation: NavigationScreenProps
}

const Groups = ({ navigation }: Props) => {
  const { navigate } = navigation
  const [selectedGroup, setSelectedGroup] = useState('')

  const _onNext = () => {
    const group = GROUPS.find(({ id }) => id === selectedGroup) || {
      events: []
    }
    const { events } = group
    navigate('Events', { events })
  }

  return (
    <View style={styles.container}>
      {GROUPS.map(({ id }) => (
        <Button
          key={id}
          title={id}
          style={styles.button}
          selected={selectedGroup === id}
          onPress={() => setSelectedGroup(id)}
        />
      ))}
      <Button
        title="Далее"
        style={styles.next}
        disabled={selectedGroup === ''}
        onPress={_onNext}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    paddingTop: 25
  },
  button: {
    marginBottom: 20
  },
  next: {
    width: 100,
    position: 'absolute',
    bottom: 25
  }
})

Groups.navigationOptions = {
  title: 'Просмотр',
  headerTitleStyle: {
    fontWeight: 'normal',
    fontSize: 23,
    lineHeight: 27
  }
}
export default Groups
