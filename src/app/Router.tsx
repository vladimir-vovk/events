import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import Groups from './Groups'
import Events from './Events'

const App = createStackNavigator({
  Groups,
  Events
})

export default createAppContainer(App)
