export default [
  {
    id: 'Group 1',
    events: [
      {
        id: '1',
        image:
          'https://images.unsplash.com/photo-1576866206905-59e0241273b8?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2850&q=80',
        title: 'Food Tasting',
        date: 'Monday'
      },
      {
        id: '2',
        image:
          'https://images.unsplash.com/photo-1576999699226-f09c6b7395be?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjF9&auto=format&fit=crop&w=2850&q=80',
        title: 'Having Fun with Friends',
        date: 'Thurthday'
      },
      {
        id: '3',
        image:
          'https://images.unsplash.com/photo-1576990049702-8418081b420e?ixlib=rb-1.2.1&auto=format&fit=crop&w=2978&q=80',
        title: 'Playing Games',
        date: 'Sunday'
      }
    ]
  },
  {
    id: 'Group 2'
  },
  {
    id: 'Group 3'
  },
  {
    id: 'Group 4'
  }
]
