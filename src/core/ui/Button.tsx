import React from 'react'
import { TouchableOpacity, Text, StyleSheet } from 'react-native'

type Props = {
  title: string
  onPress?: () => void
  style?: any
  selected?: boolean
  disabled?: boolean
}

const Button = ({ title, style, selected, onPress, disabled }: Props) => (
  <TouchableOpacity
    disabled={disabled}
    onPress={onPress}
    style={[styles.container, style, selected && styles.selected]}
  >
    <Text>{title}</Text>
  </TouchableOpacity>
)

const styles = StyleSheet.create({
  container: {
    width: '54%',
    height: 36,
    alignItems: 'center',
    paddingTop: 8,
    paddingBottom: 9,
    paddingHorizontal: 15,
    borderColor: 'black',
    borderRadius: 20.5,
    borderWidth: StyleSheet.hairlineWidth
  },
  text: {
    fontWeight: 'normal',
    fontSize: 16,
    lineHeight: 19,
    color: 'black'
  },
  selected: {
    backgroundColor: '#FFE601'
  }
})

export default Button
